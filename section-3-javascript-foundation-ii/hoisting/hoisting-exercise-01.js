/**
 * Variable redeclaration
 * Variables defined with var are partially hoisted, which means that
 * no matter how many times a variable is redeclared it will always have the
 * value `undefined` when accessed prior to the declaration.
 */

console.log(a);

var a = "One";
var a = "Two";

/**
 * Function redeclaration
 *
 * Since functions are fully hoisted, the space in memory of the first
 * declaration will be replaced with the second declaration of b();
 */
b();

function b() {
  console.log("ABC");
}

function b() {
  console.log("DEF");
}
