var favouriteFood = "grapes";

var foodThoughts = function () {
  /**
   * favouriteFood will be set to undefined because a
   * new execution context was just created which means
   * that hoisting will happen for every variable or
   * function declared / redeclared inside the function.
   */
  console.log("Original favourite food", favouriteFood);

  var favouriteFood = "sushi";

  console.log("New favourite food: ", favouriteFood);
};

foodThoughts();
